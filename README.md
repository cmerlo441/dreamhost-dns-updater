# DreamHost DNS Updater

This program will allow you to automatically update a DNS entry in your DreamHost account if the host's IP address changes.

## How to set it up

Either run it from the command-line with a ```--hostname``` argument and a ```--key``` argument, like this:

```
python3 set-dns.py --hostname my.example.com --key my-DreamHost-API-key
```

or create a file called ```.setdnsrc``` in the home directory of whichever user will be running this program, set up like this:

```
hostname=my.example.com
key=my-DreamHost-API-key
```

## How to run it
```python3 set-dns.py```

## What it does
There's no output at all if the IP address hasn't changed, but if it changes, or if something goes wrong, it will report it to standard output.

## Usage suggestion
I run it every five minutes from cron, and capture any output in a log file:

```
*/5 * * * * python3 /path/to/set-dns.py >> /path/to/set-dns-log.txt
```