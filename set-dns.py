#! /usr/bin/python3

'''Update a DreamHost DNS entry.

Running this script on a host assigns that host's external IP address
to an existing hostname registered with DreamHost.
'''

__author__ = 'Christopher R. Merlo'
__email__ = 'cmerlo441@gmail.com'
__copyright__ = 'Copyright 2019 Christopher R. Merlo'
__license__ = 'MIT License'
__version__ = '1.0.0'
__maintainer__ = 'Christopher R. Merlo'
__status__ = 'Production'

import argparse
import re
import sys
import uuid

from pathlib import Path
from datetime import datetime
from subprocess import Popen, PIPE

# Read the rc file if it's there

home = str(Path.home())
vars = {}

hostname = None
key = None

try:
    with open(home + "/.setdnsrc") as myfile:
        for line in myfile:
            key, value = line.partition("=")[::2]
            vars[key.strip()] = value.strip()

    hostname = vars['hostname']
    key = vars['key']

except:
    pass


# Check for command line arguments
# Command line arguments can override rc file entries

parser = argparse.ArgumentParser()
parser.add_argument(
    '--hostname', help='Hostname whsoe address we\'re changing')
parser.add_argument('--key', help='DreamHost API Key')
argv = parser.parse_args()

if argv.hostname:
    hostname = argv.hostname

if argv.key:
    key = argv.key

# Need a hostname and a key to continue

if hostname is None:
    print("Exiting: no hostname specified")
    sys.exit()

if key is None:
    print("Exiting: no API key specified")
    sys.exit()

# Generate extra output
debug = 1

# Necessary settings
ifconfig = "ifconfig.me"
api_host = "https://api.dreamhost.com"

getcmd = "dns-list_records"
rmvcmd = "dns-remove_record"
setcmd = "dns-add_record"

uuid = str(uuid.uuid4())

time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')


def valid_ip(ip):
    '''Determine if the argument looks like a valid IP address
    Note: This function *does not* check if the address is actually valid,
    or even if any one section is within [0..255]
    '''
    pattern = re.compile('^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$')
    match = pattern.match(ip)
    if match:
        return True
    else:
        return False


# Get the actual external IP address
process = Popen(['curl', '-s', ifconfig], stdout=PIPE)
(output, err) = process.communicate()
exit_code = process.wait()

if exit_code == 0:
    actual_ip = output.decode('utf-8')
else:
    print(time + ": Can't get IP address from", ifconfig)
    sys.exit()

# Ask DreamHost what they think the IP address is
link_stub = api_host + "?key=" + key + "&unique_id=" + uuid + "&cmd="
link = link_stub + getcmd

process = Popen(['wget', '-O-', '-q', link], stdout=PIPE)
(output, err) = process.communicate()
exit_code = process.wait()

if exit_code == 0:
    hosts = output.decode('utf-8')
else:
    print(time + ": Can't get IP address from dreamhost")
    sys.exit()

# Prevent the if statement below from crashing if there's no response from Dreamhost
stored_ip = '0.0.0.0'

for line in hosts.splitlines():
    # print(line)
    pattern = re.compile(".*" + hostname + ".*")
    match = pattern.match(line)
    if match:
        parts = line.split()
        stored_ip = parts[4]

# See if the two IP addresses are equal
if actual_ip != stored_ip:

    # delete the current entry

    # Bug: An attempt to call uuid.uuid4() again here always crashes
    # Terribly hacky workaround: add a 1 to the end of the old UUID here
    # and add a 2 to the end below
    # Yes, I am ashamed of how ugly this kludge is

    remove_args = rmvcmd + "&record=" + hostname + "&type=A&value=" + stored_ip

    link_stub = api_host + "?key=" + key + "&unique_id=" + uuid + "1" + "&cmd="
    link = link_stub + remove_args
    process = Popen(['wget', '-O-', '-q', link], stdout=PIPE)
    (output, err) = process.communicate()
    exit_code = process.wait()

    if exit_code == 0:
        pass
    else:
        print(time + ": Can't remove IP address from dreamhost")
        sys.exit()

    # add the new entry
    # uuid3 = str(uuid.uuid4())
    set_args = setcmd + "&record=" + hostname + "&type=A&value=" + actual_ip

    link_stub = api_host + "?key=" + key + "&unique_id=" + uuid + "2" + "&cmd="
    link = link_stub + set_args
    process = Popen(['wget', '-O-', '-q', link], stdout=PIPE)
    (output, err) = process.communicate()
    exit_code = process.wait()

    if exit_code == 0:
        pass
    else:
        print(time + ": Can't set IP address on dreamhost - no DNS entry now")
        sys.exit()

    print(time)
    print("IP address changed from " + stored_ip + " to " + actual_ip)
